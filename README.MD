# Benchmark Datasets for Systematic Literature Reviews

Systematic literature reviews are conducted to provide 
a comprehensive and concise understanding of a specific
topic, identify research gaps, and potentially generate 
new ideas. However, the abstract screening phase, which 
involves assessing the relevance of a substantial 
number of documents, can be particularly time-consuming 
and labor-intensive. 
There are already several approaches to accelerate 
this process. However, it is crucial that these approaches 
are comparable in order to facilitate effective assessment.
Publicly available datasets play a vital role by providing a 
standardized benchmark for the development and evaluation of models.

Some researchers already provide prelabeled collections of datasets.
These datasets are derived from completed systematic literature reviews 
and include labels of whether the corresponding documents 
were deemed relevant or not relevant for those reviews. However, 
the format of these datasets varies across different providers.
To address this issue, this simple and straightforward Jupyter Notebook 
was developed to retrieve meta data and normalize all datasets.

# Manual Download
Please manually download the collections 
of datasets from the original authors first and
move them to the corresponding data folder.

* Cohen et al. (2006): https://dmice.ohsu.edu/cohenaa/systematic-drug-class-review-data.html
* Howard et al. (2016): https://systematicreviewsjournal.biomedcentral.com/articles/10.1186/s13643-016-0263-z#Sec30
   * Take note, that the contents of the XLXS file has to be exported first as CSV
* van de Schoot et al. (2021):  https://osf.io/2jkd6/
* P. Wang et al. (2007) : https://bmcbioinformatics.biomedcentral.com/articles/10.1186/1471-2105-8-269#MOESM2
* Yu et al. (2018) : https://doi.org/10.5281/zenodo.837298
   * The kitchenham csv file is encoded as ANSI, please manually convert it as utf-8 using e.g. notepad++
   * The Radjenoic file also seem to have some encoding problems, after encoding it to ANSI and then back to utf-8, the problem seemed to be solved

# Run the Notebook

The notebook can be executed in a sequential manner. 
In cases where datasets only provide PubMed IDs, the 
notebook will first download the corresponding abstracts. 
The inclusion labels, which may differ across datasets, 
will be normalized. Any unavailable documents stemming from PubMed IDs
will be identified and printed accordingly. The notebook also 
includes some minor initial data exploration. 
All datasets will be saved as CSV files.


# Output
The exported csv files will have the following columns:
* title: Title of the document
* abstract: Abstract of the document. Take note, that some documents might have no abstract.
* included: A flag indicating inclusion: 1= included, 0 = not included
* pubmedid: (optional) If the source contained that information.

# Cite this

If you found this work helpful, feel free to cite this using the following bibtex reference:
```bibtex
@misc{Nguyen2023BenchmarkDatasetSLR,
  author = {Nguyen, Hung Manh},
  title = {Benchmark Datasets for Systematic Literature Reviews},
  year = {2023},
  publisher = {GitLab},
  journal = {GitLab repository},
  howpublished = {\url{https://gitlab.com/nm_hung93/benchmark-datasets-for-systematic-literature-reviews}},
}
```

# References

Cohen, A. M., Hersh, W. R., Peterson, K., & Yen, P.-Y. (2006). Reducing Workload in Systematic Review Preparation Using Automated Citation Classification. Journal of the American Medical Informatics Association, 13(2), 206-219. https://doi.org/10.1197/jamia.M1929

Howard, B. E., Phillips, J., Miller, K., Tandon, A., Mav, D., Shah, M. R., Holmgren, S., Pelch, K. E., Walker, V., Rooney, A. A., Macleod, M., Shah, R. R., & Thayer, K. (2016). SWIFT-Review: A text-mining workbench for systematic review. Systematic Reviews, 5(1), 87. https://doi.org/10.1186/s13643-016-0263-z

Howard, B. E., Phillips, J., Tandon, A., Maharana, A., Elmore, R., Mav, D., Sedykh, A., Thayer, K., Merrick, B. A., Walker, V., Rooney, A., & Shah, R. R. (2020). SWIFT-Active Screener: Accelerated document screening through active learning and integrated recall estimation. Environment International, 138, 105623. https://doi.org/10.1016/j.envint.2020.105623

van de Schoot, R., de Bruin, J., Schram, R., Zahedi, P., de Boer, J., Weijdema, F., Kramer, B., Huijts, M., Hoogerwerf, M., Ferdinands, G., Harkema, A., Willemsen, J., Ma, Y., Fang, Q., Hindriks, S., Tummers, L., & Oberski, D. L. (2021). An open source machine learning framework for efficient and transparent systematic reviews. Nature Machine Intelligence, 3(2), 125-133. https://doi.org/10.1038/s42256-020-00287-7

Wang, P., Morgan, A. A., Zhang, Q., Sette, A., & Peters, B. (2007). Automating document classification for the Immune Epitope Database. BMC Bioinformatics, 8(1), 269. https://doi.org/10.1186/1471-2105-8-269

Yu, Z., Kraft, N. A., & Menzies, T. (2018). Finding Better Active Learners for Faster Literature Reviews. Empirical Software Engineering, 23(6), 3161-3186. https://doi.org/10.1007/s10664-017-9587-0