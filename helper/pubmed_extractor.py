import math

import numpy as np
import pandas as pd
from Bio import Entrez



# https://stackoverflow.com/questions/47559098/is-there-any-way-to-get-abstracts-for-a-given-list-of-pubmed-ids
class PubmedExtractor:

    def __init__(self,email):
        """
        :param email: Email address to use NCBI's e-utilities
        :param email: str
        """

        Entrez.email = email

    def __call__(self, pubmedids):
        return self.extract_pubmedis(pubmedids)

    def extract_pubmedis(self, pubmedids):
        """

        :param pubmedids: List of all pubmedids
        :type pubmedids: list
        :param email: Email address to use NCBI's e-utilities
        :param email: str
        :return:
        :rtype: pd.DataFrame

        """

        max_iterations = math.ceil(len(pubmedids)/10000)
        all_abstracts = []
        all_titles = []
        all_pmids = []

        print(f"Starting extraction of {len(pubmedids)} pubmedids")
        for i in range(0,max_iterations):
            retstart = i*10_000
            # efetch documentation: https://www.ncbi.nlm.nih.gov/books/NBK25499/#!po=13.6364
            # retrieval max is set to 10.000, to retrieve more than that limit, iterate using retstart
            handle = Entrez.efetch(db="pubmed", id=','.join(map(str, pubmedids)),
                               rettype="xml", retmode="text",  retmax="10000", retstart=retstart)
            records = Entrez.read(handle)
            abstracts = [pubmed_article['MedlineCitation']['Article']['Abstract']['AbstractText'][0] if 'Abstract' in pubmed_article['MedlineCitation']['Article'].keys() else np.NaN  for pubmed_article in records['PubmedArticle']]
            titles = [pubmed_article['MedlineCitation']['Article']['ArticleTitle'] for pubmed_article in records['PubmedArticle']]
            pmids = [pubmed_article['MedlineCitation']['PMID'].split(',')[0] for pubmed_article in records['PubmedArticle']]
            all_abstracts.extend(abstracts)
            all_titles.extend(titles)
            all_pmids.extend(pmids)
            print(f"Iteration {i}")

        pubmed_extract_df = pd.DataFrame(list(zip(all_pmids, all_titles, all_abstracts)),
                     columns=['PMID', 'title', 'abstract'])

        # https://stackoverflow.com/questions/21197774/assign-pandas-dataframe-column-dtypes
        # for the join to work, the datatypes have to be specified for the columns
        pubmed_extract_df = pubmed_extract_df.astype(dtype={'PMID': 'int', 'title': 'string', 'abstract': object})


        return pubmed_extract_df